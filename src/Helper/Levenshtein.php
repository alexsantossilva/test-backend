<?php

namespace ShoppingList\Helper;

use ShoppingList\Enum\Products;

/**
 * Class Levenshtein
 * @package ShoppingList\Helper
 */
class Levenshtein
{
    /**
     * @param $input
     * @return mixed
     */
    public static function closestWord($input) {
        $shortest = -1;
        foreach (Products::$mappingWords as $word) {
            $lev = levenshtein($input, $word);

            if ($lev == 0) {
                $closest = $word;
                $shortest = 0;
                break;
            }

            if ($lev <= $shortest || $shortest < 0) {
                $closest  = $word;
                $shortest = $lev;
            }
        }

        return $closest;
    }
}
