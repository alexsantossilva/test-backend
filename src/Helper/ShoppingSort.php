<?php

namespace ShoppingList\Helper;

use ShoppingList\Enum\Category;

/**
 * Class ShoppingSort
 * @package ShoppingList\Helper
 */
class ShoppingSort
{
    /**
     * @param array $data
     * @return array
     */
    public static function monthSort(array $data): array
    {
        $result = [];
        foreach ($data as $key => $item) {
            $monthNumber = Month::getMonthNumber($key);
            $result[$monthNumber] = $item;
        }
        ksort($result);
        return $result;
    }

    /**
     * @param array $data
     * @return array
     */
    public static function categorySort(array $data): array
    {
        $result = [];
        foreach ($data as $key => $item) {
            ksort($item);
            $result[$key] = $item;
        }

        return $result;
    }

    /**
     * @param array $data
     * @return array
     */
    public static function quantitySort(array $data): array
    {
        foreach ($data as $key => $items) {
            foreach (Category::$mapping as $categoryKey => $products) {
                asort($data[$key][$categoryKey]);
            }
        }

        return $data;
    }

    /**
     * @param array $data
     * @return array
     */
    public static function removeEmptyValue(array $data): array
    {
        foreach ($data as $key => $category) {
            foreach ($category as $keyItem => $item) {
                if (count($item) == 0) {
                    unset($data[$key]);
                    continue;
                }
            }
        }

        return $data;
    }
}
