<?php

namespace ShoppingList\Helper;

/**
 * Class Month
 * @package ShoppingList\Helper
 */
class Month
{
    const JANUARY = "Janeiro";
    const FEBRUARY = "Fevereiro";
    const MARCH = "Marco";
    const APRIL = "Abril";
    const MAY = "Maio";
    const JUNE = "Junho";
    const JULY = "Julho";
    const AUGUST = "Agosto";
    const SEPTEMBER = "Setembro";
    const OCTOBER = "Outubro";
    const NOVEMBER = "Novembro";
    const DECEMBER = "Dezembro";

    /**
     * @var array
     */
    private static $monthMapping = [
        "01" => self::JANUARY,
        "02" => self::FEBRUARY,
        "03" => self::MARCH,
        "04" => self::APRIL,
        "05" => self::MAY,
        "06" => self::JUNE,
        "07" => self::JULY,
        "08" => self::AUGUST,
        "09" => self::SEPTEMBER,
        "10" => self::OCTOBER,
        "11" => self::NOVEMBER,
        "12" => self::DECEMBER,
    ];

    /**
     * @param $month
     * @return string
     */
    public static function getMonthNumber($month)
    {
        $month = ucfirst(strtolower($month));

        return array_search($month, self::$monthMapping);
    }

    /**
     * @param $number
     * @return string
     */
    public static function getMonth($number)
    {
        return isset(self::$monthMapping[$number]) ? self::$monthMapping[$number] : $number;
    }
}
