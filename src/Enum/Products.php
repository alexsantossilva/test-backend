<?php

namespace ShoppingList\Enum;

/**
 * Class Products
 * @package ShoppingList\Enum
 */
class Products
{
    /**
     * Pode ter mais palavras / Seria mais fácil criar um template de tradução
     *
     * @var array
     */
    public static $mappingWords = [
        'Papel Higiênico',
        'Geléia de morango',
        'Sabão em pó',
        'Chocolate ao leite',
        'Brócolis',
        'Pão de forma',
        'Nutella',
        'Arroz',
        'Feijão',
        'Veja multiuso',
        'Desinfetante',
        'Creme dental',
        'Sabonete Protex',
        'Escova de dente',
        'Ovos',
        'Iogurte',
        'Pasta de Amendoim',
        'Detergente',
        'Enxaguante bocal',
        'Tomate',
        'Morango',
        'Berinjela',
        'Pepino',
        'Arroz integral',
        'Feijão',
        'Filé de frango',
        'Leite desnatado',
        'Diabo verde',
        'MOP',
        'Shampoo',
        'Pão de forma',
        'Queijo minas',
        'Geléia de morango',
        'Pano de chão',
        'Rodo',
        'Creme dental',
        'Sabonete Dove',
        'Doritos',
        'Esponja de aço',
        'Fio dental',
        'Escova de dente',
    ];
}
