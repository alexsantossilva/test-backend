<?php

namespace ShoppingList\Enum;

/**
 * Class Category
 * @package ShoppingList\Enum
 */
class Category
{
    const ALIMENTOS = "Alimentos";
    const HIGIENE = "Higiene";
    const LIMPEZA = "Limpeza";

    /**
     * @var array
     */
    public static $mapping = [
        "alimentos" => self::ALIMENTOS,
        "higiene_pessoal" => self::HIGIENE,
        "limpeza" => self::LIMPEZA,
    ];
}
