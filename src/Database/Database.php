<?php

namespace ShoppingList\Database;

/**
 * Interface Database
 * @package ShoppingList\Database
 */
interface Database
{
    /**
     * @return mixed
     */
    public function findAll();

    /**
     * @return mixed
     */
    public function insert(array $data);
}