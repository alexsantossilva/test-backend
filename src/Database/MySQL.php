<?php

namespace ShoppingList\Database;

/**
 * Class MySQL
 * @package ShoppingList\Database
 */
class MySQL
{
    private $host = "localhost";
    private $port = "3306";
    private $user = "user";
    private $pass = "password";
    private $db = "akna_teste";

    private $pdo;

    /**
     * @return void
     */
    private function connect()
    {
        $dsn = "mysql:host={$this->host};port={$this->port};dbname={$this->db}";
        $this->pdo = new \PDO($dsn, $this->user, $this->pass);
    }

    /**
     * @return \PDO
     */
    public function getPdo()
    {
        if ($this->pdo instanceof \PDO) {

            return $this->pdo;
        }
        $this->connect();

        return $this->pdo;
    }
}
