<?php

namespace ShoppingList;

use ShoppingList\Helper\Levenshtein;
use ShoppingList\Helper\Month;
use ShoppingList\Helper\ShoppingSort;
use ShoppingList\Enum\Category;
use ShoppingList\Model\ShoppingList;

/**
 * Class Client
 * @package ShoppingList
 */
class Client
{
    /**
     * @param array $data
     * @throws \Exception
     */
    public function build(array $data)
    {
        $data = $this->normalize($data);
        echo "Generating CSV... \n";
        $this->generateCsv($data);
        echo "CSV [OK] \n";
        echo "Saving in Database Table shopping_list... \n";
        $this->saveDatabase($data);
        echo "Insert Table [OK]\n";
    }

    /**
     * @param array $data
     */
    protected function generateCsv(array $data)
    {
        $fp = fopen('compras-do-ano.csv', 'w');

        foreach ($data as $fields) {
            fputcsv($fp, $fields);
        }
    }

    /**
     * @param array $data
     * @throws \Exception
     */
    protected function saveDatabase(array $data)
    {
        $model = new ShoppingList();
        $model->insert($data);
    }

    /**
     * @param array $data
     * @return array
     */
    protected function normalize(array $data): array
    {
        $list = ShoppingSort::removeEmptyValue($data);
        $list = ShoppingSort::monthSort($list);
        $list = ShoppingSort::categorySort($list);
        $list = ShoppingSort::quantitySort($list);

        $result = [];
        foreach ($list as $key => $fields) {
            $month = Month::getMonth($key);
            foreach (Category::$mapping as $categoryKey => $category) {
                foreach ($fields[$categoryKey] as $itemName => $value) {
                    $result[] = [
                        $month,
                        $category,
                        Levenshtein::closestWord($itemName),
                        $value,
                    ];
                }
            }
        }

        return $result;
    }
}



