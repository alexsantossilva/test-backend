<?php

namespace ShoppingList\Model;

use ShoppingList\Database\Database;
use ShoppingList\Database\MySQL;

/**
 * Class ShoppingList
 * @package ShoppingList\Model
 */
class ShoppingList implements Database
{
    const TABLE = "shopping_list";

    /**
     * @var MySQL
     */
    private $mysql;

    /**
     * ShoppingList constructor.
     */
    public function __construct()
    {
        $this->mysql = new MySQL();
    }

    /**
     * @return array|mixed
     */
    public function findAll()
    {
        $query = "SELECT * FROM" . self::TABLE;
        $stm = $this->mysql->getPdo()->prepare($query);

        return $stm->fetchAll();
    }

    /**
     * @param array $data
     * @return mixed|void
     * @throws \Exception
     */
    public function insert(array $data)
    {
        $sql = "INSERT INTO " . self::TABLE . " (month, category, product, quantity) VALUES (?, ?, ?, ?)";
        $stmt = $this->mysql->getPdo()->prepare($sql);
        try {
            $this->mysql->getPdo()->beginTransaction();
            foreach ($data as $row) {
                $stmt->execute($row);
            }
            $this->mysql->getPdo()->commit();
        } catch (\Exception $e){
            $this->mysql->getPdo()->rollback();
            throw $e;
        }
    }
}
