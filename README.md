# Akna Software - Analista Programador PHP

## Instruções para publicação

1. Para publicar seu projeto você deverá fazer um fork desse repositório.
2. Utilize o aquivo "lista-de-compras.php" como estrutura de dados.
3. Seu teste deve ser executável via linha de comando.
4. Seu teste deve conter um arquivo .sql que crie o banco de dados e todas as tabelas necessárias.
5. Você deve usar a versão 7.2 do PHP.
6. Você deve usar a versão 5.5 do MySQL.
7. Você pode usar o framework Laravel ou fazer apenas com PHP puro.

# O teste

1. Rodar o Composer install
```
$ composer install
```
2. Criar o database akna_teste. O arquivo com o dump está como:
`akna_teste.sql`

3. Rodar o teste e será criado o CSV junto com o Insert no Banco
```
$ php akna.php
```

* Para configurar o Database:
`src\Database\MySQL.php`

Thanks ;)

 

 
