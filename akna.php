<?php

use ShoppingList\Client;

include "vendor/autoload.php";

function requireShoppingList($file)
{
    return require ($file);
}

$file = "lista-de-compras.php";
$shoppingList = requireShoppingList($file);

$client = new Client();
$client->build($shoppingList);
